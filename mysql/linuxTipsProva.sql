create database linuxtips;
use linuxtips;
create table pessoas (
  id int not null auto_increment primary key,
  nome varchar(255),
  idade int
);

GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'linuxtips' WITH GRANT OPTION;
